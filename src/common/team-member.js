import React from "react";
import { ListItem, Avatar } from "react-native-elements";

export const TeamMember = ({ name, role }) => (
  <ListItem bottomDivider>
    <Avatar rounded title={name.split(' ').map(n => n[0]).join('')} containerStyle={{ backgroundColor: '#007aff'  }} />
    <ListItem.Content>
      <ListItem.Title>{name}</ListItem.Title>
      <ListItem.Subtitle style={{ color: '#999' }}>{role}</ListItem.Subtitle>
    </ListItem.Content>
  </ListItem>
);
