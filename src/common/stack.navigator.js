import React from "react";
import { Icon } from "react-native-elements";
import { TouchableOpacity } from "react-native";
import { DrawerActions } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export const StackNavigator = ({ title, component }) => (
  <Stack.Navigator>
    <Stack.Screen
      name={title}
      component={component}
      options={({ navigation }) => ({
        headerTitle: title,
        headerLeft: () => (
          <TouchableOpacity
            onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}
          >
            <Icon
              type="font-awesome"
              style={{ marginLeft: 10 }}
              name="bars"
              size={25}
            />
          </TouchableOpacity>
        ),
      })}
    />
  </Stack.Navigator>
);
