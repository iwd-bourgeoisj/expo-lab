import React from "react";
import { ScrollView } from "react-native";
import { StackNavigator } from "../common/stack.navigator";
import { TeamMember } from "../common/team-member";

const LeadershipTeamView = () => (
  <ScrollView>
    <TeamMember name="Greg Betty" role="President &amp; CEO" />
    <TeamMember name="Bruno Schmidt" role="Senior Vice President, Technology" />
    <TeamMember name="Greg McKenzie" role="Senior Vice President, Operations" />
    <TeamMember name="Andrew Pitt" role="Senior Vice President, Delivery" />
    <TeamMember name="Stephanie Clarkson" role="Director, People Team" />
    <TeamMember name="Keith Shiner" role="Vice President, Delivery" />
    <TeamMember name="Ben Hall" role="Vice President, Technology" />
  </ScrollView>
);

export const LeadershipTeamScreen = () => (
  <StackNavigator title="Delivery Team" component={LeadershipTeamView} />
);
