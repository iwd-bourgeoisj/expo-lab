import React from "react";
import { StackNavigator } from "../common/stack.navigator";
import { TeamMember } from "../common/team-member";
import { ScrollView } from "react-native";

const DeliveryTeamView = () => (
  <ScrollView>
    <TeamMember name="Alan B" role="developer" />
    <TeamMember name="Chris D" role="developer" />
    <TeamMember name="Eva F " role="developer" />
    <TeamMember name="Greg H " role="developer" />
  </ScrollView>
);

export const DeliveryTeamScreen = () => (
  <StackNavigator title="Delivery Team" component={DeliveryTeamView} />
);
