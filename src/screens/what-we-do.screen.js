import React from "react";
import { Text } from "react-native";
import { ScrollView } from "react-native";
import { Card } from "react-native-elements";
import { StackNavigator } from "../common/stack.navigator";

const WhatWeDoView = () => (
  <ScrollView style={{ textAlign: "center" }}>
    <Card>
      <Card.Title>Replatforming &amp; Modernization</Card.Title>
      <Card.Divider />
      <Text>
        Legacy systems maintenance chew up to 80% of most enterprise IT budgets.
        Chip away at that by Modernizing your legacy platforms.
      </Text>
    </Card>

    <Card>
      <Card.Title>Enterprise Solutions</Card.Title>
      <Card.Divider />
      <Text>
        We develop software that enables organizations in complex industries to
        achieve their objectives.
      </Text>
    </Card>

    <Card>
      <Card.Title>Agile Adoption Services</Card.Title>
      <Card.Divider />
      <Text>
        Transition your organization to modern, effective Agile practices. We
        have helped organizations of all sizes take the leap to Agile.
      </Text>
    </Card>

    <Card>
      <Card.Title>Fintech Platforming</Card.Title>
      <Card.Divider />
      <Text>
        Compete substantially from the start with an MVP built from the
        ground-up to scale.
      </Text>
    </Card>

    <Card>
      <Card.Title>IT Strategy</Card.Title>
      <Card.Divider />
      <Text>
        Actionable strategies begin with experienced advice. We can help you get
        unstuck.
      </Text>
    </Card>

    <Card>
      <Card.Title>Product Development</Card.Title>
      <Card.Divider />
      <Text>
        Turn your product ideas into great market opportunities. Partner with
        developers and UX specialists committed to delivery.
      </Text>
    </Card>
  </ScrollView>
);

export const WhatWeDoScreen = () => <StackNavigator title="What we do" component={WhatWeDoView} />;
