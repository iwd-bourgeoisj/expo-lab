import React from "react";
import { Icon } from "react-native-elements";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { DeliveryTeamScreen } from "./delivery-team.screen";
import { LeadershipTeamScreen } from "./leadership-team.screen";

const Tab = createBottomTabNavigator();

export const OurTeamScreen = () => (
  <Tab.Navigator>
    <Tab.Screen
      name="Delivery Team"
      options={{
        tabBarIcon: () => <Icon type="font-awesome" name="users" />,
      }}
      component={DeliveryTeamScreen}
    />
    <Tab.Screen
      name="Leadership Team"
      component={LeadershipTeamScreen}
      options={{
        tabBarIcon: () => <Icon type="font-awesome" name="info-circle" />,
      }}
    />
  </Tab.Navigator>
);
