
# Universal React application - Lab - Web COE

## Intro

The goal of this lab is to build an app that can run on web, iOS and Android. To keep it simple we're going to do so using <https://expo.io/>, however, it could also be done with just `React Native` or even `Flutter`. 

Unlike what we tradionally do, build for web and then make it mobile friendly using responsive design, here we're going to build native first and get web for "free".

**Important:** Make sure to have `node LTS` or newer installed

## Activities

### 1. Create an Expo application

- `npm install --global expo-cli`
- `expo init --name expo-lab --template bare-minimum`
- `cd expo-lab`
- `npm run web`

> Edit `App.js` and watch the app auto refresh.

### 2. Run on mobile

- Install `Expo Go` on you device - <https://expo.io/client>
- Scan the QR code on the terminal and one the link with `Expo Go`

> Edit again `App.js` and watch the app auto refresh on both web and mobile.

### 3. Configure app icon and splash screen

- Edit `app.json` to configure the app icon and the splash screen - <https://docs.expo.io/guides/splash-screens/>
- Kill the app and re-open

Note: Feel free to grab the assets from the repository on gitlab.

### 4. Install UI Elemts and Navigation packages

For today's session, we'll use:
- <https://reactnativeelements.com/> - Routing and navigation for Expo and React Native apps.
- <https://reactnavigation.org/> - Cross Platform React Native UI Toolkit


Installation steps:
- `npm install --save react-native-elements react-native-safe-area-context @react-navigation/native @react-navigation/bottom-tabs @react-navigation/stack`
- `expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view`

Replace the `App.js` with:

```
import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

function HomeScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Home!</Text>
    </View>
  );
}

function SettingsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Home" component={HomeScreen} />
        <Tab.Screen name="Settings" component={SettingsScreen} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
```

### 5. Create and populate `Our team` page

- Rename the tabs to `Delivery Team` and `Leadership Team`
- Use `Stack Navigator` to enable header
- Populate screens with team members

Documentation: <https://reactnavigation.org/docs/stack-navigator>

### 6. Add a drawer menu

- Add a drawer menu to the application
- Add 2 links to the drawer one for `Our team` and one for `What we do`
- Populate `What we do` screen with data

Documentation: <https://reactnavigation.org/docs/drawer-based-navigation/>


## Going Further

7. Add a page to show details about a leadership team member using `Stack Navigator`
8. Add a page to show details about a delivery team member using `Modal`
9. Have different layout on different devices (unknown territory)
10. Add an authentication flows
