import React from "react";
import { StatusBar } from "expo-status-bar";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { ThemeProvider } from "react-native-elements";
import { WhatWeDoScreen } from "./src/screens/what-we-do.screen";
import { OurTeamScreen } from "./src/screens/our-team.screen";

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <ThemeProvider
      theme={{ colors: { black: '#007aff' } }}
    >
      <SafeAreaProvider>
        <StatusBar style="auto" />
        <NavigationContainer>
          <Drawer.Navigator initialRouteName="Team">
            <Drawer.Screen name="Our Team" component={OurTeamScreen} />
            <Drawer.Screen name="What we do" component={WhatWeDoScreen} />
          </Drawer.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </ThemeProvider>
  );
}
